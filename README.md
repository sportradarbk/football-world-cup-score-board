# Football World Cup Score Board

This library provides an implementation of a live football world cup score board.

## Features

- Start a game with homeTeam VS awayTeam with initial score 0-0
- Finish a game
- Update scores
- Get a summary of games by total score, with games of the same score ordered by recency

## Usage

1. Clone the repository.
2. Run the unit tests to ensure everything is working as expected.
3. Use the `ScoreBoard` to manage the football world cup score board.

## How to run the tests

```sh
   ./gradlew test 
```

## Assumptions

- The game scores are always updated with valid integers.
- There are no duplicate games (each game is unique by its home and away teams).
- The `gameStartTime` is used to determine the recency of the game additions.
- `Game` interface will allow to integrate seamlessly different types of other games (f.e. BasketballGame)
- `ScoreBoard` interface will allow to integrate seamlessly different types of other cups (f.e. BasketballEuroCupBoard)

## Dependencies

- JUnit 5 for unit testing.