package io.sportradar.board;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.sportradar.exception.ScoreBoardException;
import io.sportradar.game.Game;
import io.sportradar.model.Team;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FootballWorldCupScoreBoardTest {

	private static final String MEXICO = "Mexico";
	private static final String CANADA = "Canada";
	private static final String SPAIN = "Spain";
	private static final String BRAZIL = "Brazil";
	private static final String GERMANY = "Germany";
	private static final String FRANCE = "France";
	private static final String URUGUAY = "Uruguay";
	private static final String ITALY = "Italy";
	private static final String ARGENTINA = "Argentina";
	private static final String AUSTRALIA = "Australia";


	private ScoreBoard underTest;

	@BeforeEach
	void setUp() {
		Collection<Game> footballGames = new ArrayList<>();
		underTest = new FootballWorldCupScoreBoard(footballGames);
	}

	@Test
	@DisplayName("Start Football Game")
	void itShouldStartGame() {
		Team homeTeam = new Team(MEXICO);
		Team awayTeam = new Team(CANADA);

		underTest.startGame(homeTeam, awayTeam);

		assertFalse(underTest.getAllGames().isEmpty());
		Optional<Game> game = underTest.getAllGames().stream().findFirst();
		assertTrue(game.isPresent());
		assertEquals(MEXICO, game.get().getHomeTeam().getName());
		assertEquals(0, game.get().getHomeTeam().getScore());
		assertEquals(CANADA, game.get().getAwayTeam().getName());
		assertEquals(0, game.get().getAwayTeam().getScore());
	}

	@Test
	@DisplayName("Impossible To Start Football Game Once Again")
	void itShouldNotStartGameOneMoreTime() {
		Team homeTeam = new Team(MEXICO);
		Team awayTeam = new Team(CANADA);

		Team homeTeam1 = new Team(MEXICO);
		Team awayTeam1 = new Team(CANADA);

		underTest.startGame(homeTeam, awayTeam);

		assertThrows(ScoreBoardException.class, () -> underTest.startGame(homeTeam1, awayTeam1));
	}

	@Test
	@DisplayName("Finish Football Game")
	void itShouldFinishGame() {
		Team homeTeam = new Team(MEXICO);
		Team awayTeam = new Team(CANADA);

		underTest.startGame(homeTeam, awayTeam);

		underTest.finishGame(homeTeam, awayTeam);

		assertTrue(underTest.getActiveGames().isEmpty());
		Optional<Game> deletedGame = underTest.getAllGames().stream().findFirst();
		assertTrue(deletedGame.isPresent());
		assertFalse(deletedGame.get().isActive());
	}

	@Test
	@DisplayName("Attempt To Finish Not Active Football Game")
	void itShouldNotFinishGame() {
		Team homeTeam = new Team(MEXICO);
		Team awayTeam = new Team(CANADA);

		underTest.startGame(homeTeam, awayTeam);

		underTest.finishGame(homeTeam, awayTeam);

		assertThrows(ScoreBoardException.class, () -> underTest.finishGame(homeTeam, awayTeam));
	}

	@Test
	@DisplayName("Update Score For Football Game, homeTeam - awayTeam: 1 - 2")
	void itShouldUpdateScoreGame() {
		Team homeTeam = new Team(MEXICO);
		Team awayTeam = new Team(CANADA);

		underTest.startGame(homeTeam, awayTeam);

		homeTeam.setScore(1);
		underTest.updateScore(homeTeam, awayTeam);

		awayTeam.setScore(1);
		underTest.updateScore(homeTeam, awayTeam);
		awayTeam.setScore(2);
		underTest.updateScore(homeTeam, awayTeam);

		assertEquals(1, homeTeam.getScore());
		assertEquals(2, awayTeam.getScore());
	}

	@Test
	@DisplayName("Update Score For Not Existing Football Game")
	void itShouldNotUpdateScoreGame() {
		Team homeTeam = new Team(MEXICO);
		Team awayTeam = new Team(CANADA);

		underTest.startGame(homeTeam, awayTeam);

		Team homeTeam1 = new Team(SPAIN);
		Team awayTeam1 = new Team(BRAZIL);

		homeTeam1.setScore(1);
		assertThrows(ScoreBoardException.class, () -> underTest.updateScore(homeTeam1, awayTeam1));

		assertEquals(0, homeTeam.getScore());
		assertEquals(0, awayTeam.getScore());
	}

	@Test
	@DisplayName("Update Score With Negative Values For Football Game")
	void itShouldNotUpdateNegativeValueScoreGame() {
		Team homeTeam = new Team(MEXICO);
		Team awayTeam = new Team(CANADA);

		underTest.startGame(homeTeam, awayTeam);
		underTest.updateScore(homeTeam, awayTeam);

		assertThrows(ScoreBoardException.class, () -> homeTeam.setScore(-3));
		Optional<Game> game = underTest.getActiveGames().stream().findFirst();
		assertTrue(game.isPresent());
		assertEquals(0, game.get().getHomeTeam().getScore());
		assertEquals(0, game.get().getAwayTeam().getScore());
	}

	@Test
	@DisplayName("Get Games Summary Ordered By Total Score And Game Start Time")
	void testGetSummary() {
		Team team1 = new Team(MEXICO);
		Team team2 = new Team(CANADA);
		Team team3 = new Team(SPAIN);
		Team team4 = new Team(BRAZIL);
		Team team5 = new Team(GERMANY);
		Team team6 = new Team(FRANCE);
		Team team7 = new Team(URUGUAY);
		Team team8 = new Team(ITALY);
		Team team9 = new Team(ARGENTINA);
		Team team10 = new Team(AUSTRALIA);

		underTest.startGame(team1, team2);
		team1.setScore(0);
		team2.setScore(5);
		underTest.updateScore(team1, team2);

		underTest.startGame(team3, team4);
		team3.setScore(10);
		team4.setScore(2);
		underTest.updateScore(team3, team4);

		underTest.startGame(team5, team6);
		team5.setScore(2);
		team6.setScore(2);
		underTest.updateScore(team5, team6);

		underTest.startGame(team7, team8);
		team7.setScore(6);
		team8.setScore(6);
		underTest.updateScore(team7, team8);

		underTest.startGame(team9, team10);
		team9.setScore(3);
		team10.setScore(1);
		underTest.updateScore(team9, team10);

		List<Game> summary = underTest.getSummary();
		assertEquals(URUGUAY, summary.get(0).getHomeTeam().getName());
		assertEquals(ITALY, summary.get(0).getAwayTeam().getName());
		assertEquals(6, summary.get(0).getHomeTeam().getScore());
		assertEquals(6, summary.get(0).getAwayTeam().getScore());

		assertEquals(SPAIN, summary.get(1).getHomeTeam().getName());
		assertEquals(BRAZIL, summary.get(1).getAwayTeam().getName());
		assertEquals(10, summary.get(1).getHomeTeam().getScore());
		assertEquals(2, summary.get(1).getAwayTeam().getScore());

		assertEquals(MEXICO, summary.get(2).getHomeTeam().getName());
		assertEquals(CANADA, summary.get(2).getAwayTeam().getName());
		assertEquals(0, summary.get(2).getHomeTeam().getScore());
		assertEquals(5, summary.get(2).getAwayTeam().getScore());

		assertEquals(ARGENTINA, summary.get(3).getHomeTeam().getName());
		assertEquals(AUSTRALIA, summary.get(3).getAwayTeam().getName());
		assertEquals(3, summary.get(3).getHomeTeam().getScore());
		assertEquals(1, summary.get(3).getAwayTeam().getScore());

		assertEquals(GERMANY, summary.get(4).getHomeTeam().getName());
		assertEquals(FRANCE, summary.get(4).getAwayTeam().getName());
		assertEquals(2, summary.get(4).getHomeTeam().getScore());
		assertEquals(2, summary.get(4).getAwayTeam().getScore());
	}


}
