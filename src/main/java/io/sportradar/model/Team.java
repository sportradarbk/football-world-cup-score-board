package io.sportradar.model;

import io.sportradar.exception.ScoreBoardException;
import java.util.Objects;

public class Team {

	private final String name;
	private int score;

	public Team(String name) {
		this.name = name;
		this.score = 0;
	}

	public String getName() {
		return name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		if (score < 0) {
			throw new ScoreBoardException("Score value can not be negative");
		}

		this.score = score;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Team team = (Team) o;
		return Objects.equals(name, team.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}
}
