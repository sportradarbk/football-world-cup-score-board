package io.sportradar.board;

import io.sportradar.exception.ScoreBoardException;
import io.sportradar.game.FootballGame;
import io.sportradar.game.Game;
import io.sportradar.model.Team;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class FootballWorldCupScoreBoard implements ScoreBoard {

	private final Collection<Game> footballGames;

	public FootballWorldCupScoreBoard(Collection<Game> footballGames) {
		this.footballGames = footballGames;
	}

	@Override
	public void startGame(Team homeTeam, Team awayTeam) {
		if (findActiveGame(homeTeam, awayTeam).isPresent()) {
			throw new ScoreBoardException(String.format("ScoreBoard already contains active game with homeTeam: %s and awayTeam: %s",
					homeTeam.getName(), awayTeam.getName()));
		}

		footballGames.add(new FootballGame(homeTeam, awayTeam, LocalDateTime.now()));
	}

	@Override
	public void finishGame(Team homeTeam, Team awayTeam) {
		Optional<Game> activeGame = findActiveGame(homeTeam, awayTeam);
		if (activeGame.isEmpty()) {
			throw new ScoreBoardException(String.format("ScoreBoard does not contain active game with homeTeam: %s and awayTeam: %s",
					homeTeam.getName(), awayTeam.getName()));
		}

		activeGame.get().setActive(false);
	}

	@Override
	public void updateScore(Team homeTeam, Team awayTeam) {
		Optional<Game> activeGame = findActiveGame(homeTeam, awayTeam);
		if (activeGame.isEmpty()) {
			throw new ScoreBoardException(String.format("ScoreBoard does not contain active game with homeTeam: %s and awayTeam: %s",
					homeTeam.getName(), awayTeam.getName()));
		}

		activeGame.get().getHomeTeam().setScore(homeTeam.getScore());
		activeGame.get().getAwayTeam().setScore(awayTeam.getScore());
	}

	@Override
	public List<Game> getSummary() {
		return getActiveGames().stream()
				.sorted(
						Comparator.comparingInt(Game::getTotalScore).reversed()
								.thenComparing(Comparator.comparing(Game::getGameStartTime).reversed()))
				.toList();
	}

	@Override
	public Collection<Game> getAllGames() {
		return footballGames;
	}

	@Override
	public Collection<Game> getActiveGames() {
		if (footballGames == null || footballGames.isEmpty()) {
			return Collections.emptyList();
		}

		return footballGames.stream()
				.filter(Game::isActive)
				.toList();
	}

	private Optional<Game> findActiveGame(Team homeTeam, Team awayTeam) {
		return this.getActiveGames().stream()
				.filter(game -> game.getHomeTeam().equals(homeTeam) && game.getAwayTeam().equals(awayTeam))
				.findFirst();
	}
}
