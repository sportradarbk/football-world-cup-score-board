package io.sportradar.board;

import io.sportradar.game.Game;
import io.sportradar.model.Team;
import java.util.Collection;
import java.util.List;

public interface ScoreBoard {

	void startGame(Team homeTeam, Team awayTeam);

	void finishGame(Team homeTeam, Team awayTeam);

	void updateScore(Team homeTeam, Team awayTeam);

	List<Game> getSummary();

	Collection<Game> getAllGames();

	Collection<Game> getActiveGames();
}
