package io.sportradar.game;

import io.sportradar.model.Team;
import java.time.LocalDateTime;
import java.util.Objects;

public class FootballGame implements Game {

	private final Team homeTeam;
	private final Team awayTeam;
	private final LocalDateTime gameStartTime;
	private boolean active;

	public FootballGame(Team homeTeam, Team awayTeam, LocalDateTime gameStartTime) {
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.gameStartTime = gameStartTime;
		this.active = true;
	}

	@Override
	public Team getHomeTeam() {
		return homeTeam;
	}

	@Override
	public Team getAwayTeam() {
		return awayTeam;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public LocalDateTime getGameStartTime() {
		return gameStartTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		FootballGame that = (FootballGame) o;
		return Objects.equals(homeTeam, that.homeTeam) && Objects.equals(awayTeam, that.awayTeam);
	}

	@Override
	public int hashCode() {
		return Objects.hash(homeTeam, awayTeam);
	}
}
