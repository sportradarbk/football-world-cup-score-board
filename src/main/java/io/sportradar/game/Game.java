package io.sportradar.game;

import io.sportradar.model.Team;
import java.time.LocalDateTime;

public interface Game {

	Team getHomeTeam();

	Team getAwayTeam();

	void setActive(boolean active);

	boolean isActive();

	LocalDateTime getGameStartTime();

	default int getTotalScore() {
		return getHomeTeam().getScore() + getAwayTeam().getScore();
	}

}
